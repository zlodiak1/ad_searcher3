from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, PasswordField
from wtforms.validators import DataRequired

from app.models import Searcher


class SearchForm(FlaskForm):
    searcher = SelectField(
        'Поисковик', 
        choices=[(str(item.id), item.searcher) for item in Searcher.query.all()],
        validators=[DataRequired()]
    )
    query = StringField('Запрос', validators=[DataRequired()])
    submit = SubmitField('Найти')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class AddSearcherForm(FlaskForm):
    searcher = StringField('Поисковик', validators=[DataRequired()])
    address = StringField('Строка запроса', validators=[DataRequired()])
    submit = SubmitField('Добавить поисковик')


class AddCensoredWord(FlaskForm):
    censored_word = StringField('Запрещённое слово', validators=[DataRequired()])
    submit = SubmitField('Добавить запрещённое слово')
