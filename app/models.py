from app import db
import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login


class Query(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    query = db.Column(db.String(120))
    censored_query = db.Column(db.String(120))
    date = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Query {}>'.format(self.query) 


class CensoredWord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(32))

    def __repr__(self):
        return '<CensoredWord {}>'.format(self.word)         


class Searcher(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    searcher = db.Column(db.String(64), unique=True)
    address = db.Column(db.String(64))

    def __repr__(self):
        return '<Searcher {}>'.format(self.searcher)


class User(UserMixin, db.Model):     
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(128))    

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
