import requests
from bs4 import BeautifulSoup


class Parser:
    def __init__(self, address, censored_query, headers):
        self.url = address + censored_query
        self.headers = headers
        # print('-----------', self.url, self.headers)

    def get_results(self):
        root_html = self.get_html(self.url, self.headers)
        blocks = self.get_blocks(root_html)
        print('==--=', blocks)
        return blocks

    def get_html(self, url, headers):
        html = requests.get(url, headers).text
        return html

    def get_blocks(self, root_html):
        blocks = []

        soup = BeautifulSoup(root_html, 'lxml')
        blocks_obj = soup.find('ul', {'class': 'serp-list'}).findAll('li', {'class': 'serp-item'})
        # return blocks_obj

        for block in blocks_obj:
            blocks.append({
                link: block.find('a')
            })

        return blocks

