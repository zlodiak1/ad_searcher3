from flask_login import current_user, login_user, logout_user
from werkzeug.security import generate_password_hash
from flask import render_template, request, redirect, redirect, url_for, flash
import re

from app import app, db
from app.forms import SearchForm, LoginForm, AddSearcherForm, AddCensoredWord
from app.models import Query, Searcher, User, CensoredWord
from app.classes.parser import Parser


@app.route('/')
@app.route('/search', methods=['GET', 'POST'])
def search():
    search_results = None
    search_form = SearchForm()

    if request.method == 'POST':
        if search_form.validate_on_submit():
            query=request.form['query']
            censored_query=request.form['query']

            for item in CensoredWord.query.all():
                censored_query = re.sub(item.word, '', censored_query, flags=re.IGNORECASE)

            query = Query(query=query, censored_query=censored_query)
            db.session.add(query)
            db.session.commit()

            search_form.query.data = None

            searcher_obj = Searcher.query.filter_by(id=request.form['searcher']).first()
            if searcher_obj: 
                parser = Parser(searcher_obj.address, censored_query, app.config['HEADERS'])
                search_results = parser.get_results()

    return render_template('search.html', title='Поиск', search_form=search_form, search_results=search_results)


@app.route('/queries')
def queries():
    return render_template('queries.html', title='Запросы')


@app.route('/info')
def info():
    return render_template('info.html', title='Инфо')


@app.route('/admin', methods=['GET', 'POST'])
def admin():
    if current_user.is_authenticated:
        if request.method == 'POST':
            if request.form['submit'] == 'Добавить поисковик':
                searcher_obj = Searcher(searcher=request.form['searcher'], address=request.form['address'])
                db.session.add(searcher_obj)
                db.session.commit()
                flash('Поисковик добавлен')
            elif request.form['submit'] == 'Добавить запрещённое слово':
                censored_word_obj = CensoredWord(word=request.form['censored_word'])
                db.session.add(censored_word_obj)
                db.session.commit()
                flash('запрещённое слово добавлено')

        searchers = [(str(item.id), item.searcher, item.address) for item in Searcher.query.all()]

        add_searcher_form = AddSearcherForm()
        add_searcher_form.searcher.data = None
        add_searcher_form.address.data = None

        censored_words = [(str(item.id), item.word) for item in CensoredWord.query.all()]

        add_censored_word_form = AddCensoredWord()
        add_censored_word_form.censored_word.data = None

        return render_template(
            'admin-in.html', 
            title='Админ панель', 
            searchers=searchers, 
            censored_words=censored_words, 
            add_searcher_form=add_searcher_form,
            add_censored_word_form = add_censored_word_form
        )
    else:
        loginForm = LoginForm()
        if request.form:
            user = User.query.filter_by(username=loginForm.username.data).first()
            if user and user.check_password(loginForm.password.data):
                login_user(user)
                return redirect(url_for('admin'))
        return render_template('admin.html', title='Админ панель: авторизация', loginForm=loginForm)    


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('admin'))        
    

