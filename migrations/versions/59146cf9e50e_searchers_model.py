"""searchers model

Revision ID: 59146cf9e50e
Revises: 
Create Date: 2019-04-22 15:11:39.933129

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '59146cf9e50e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('query',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('query', sa.String(length=120), nullable=True),
    sa.Column('censored_query', sa.String(length=120), nullable=True),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('searcher',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('searcher', sa.String(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('searcher')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('searcher')
    op.drop_table('query')
    # ### end Alembic commands ###
